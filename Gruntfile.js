"use strict";

module.exports = function (grunt) {

    // Automatically load tasks defined in package.json
    // See: https://github.com/sindresorhus/load-grunt-tasks/blob/master/readme.md
    require("load-grunt-tasks")(grunt);

    grunt.initConfig({

        // Import project properties
        properties: grunt.file.readJSON("package.json"),


        // See: https://github.com/gruntjs/grunt-contrib-compass/blob/master/README.md
        compass: {
            mobile: {
                options: {
                    sassDir: "app/styles/",
                    cssDir: "app/styles/"
                }
            }
        },

        // Configure JsHint
        // See: https://github.com/gruntjs/grunt-contrib-jshint
        jshint: {
            files: ["app/scripts/*.js"]
        },

        // Configure Watch task
        // See: https://github.com/gruntjs/grunt-contrib-watch/blob/master/README.md
        watch: {
            options: {
                livereload: true
            },
            css: {
                files: ["**/*.scss"],
                tasks: ["compass"]
            },
            html: {
                files: ["**/*.html"]
            },
            js: {
                files: ["**/*.js"],
                tasks: ["jshint"]
            }
        },

        // Configure web server
        // See: https://github.com/gruntjs/grunt-contrib-connect
        connect: {
            server: {
                options: {
                    hostname: "localhost",
                    port: 8000,
                    base: "app",
                    debug: "<%= properties.settings.debug %>",
                    livereload: true,
                    open: "<%= properties.settings.openBrowserOnRun %>"
                }
            }
        }

    });


    // Default task used for development
    grunt.registerTask("default", ["connect","watch"]);

};