# Prototype Boilerplate

A boilerplate project for rapid prototype development.
Not suitable for production ready code building.

---

# What's Included

- Basic HTML template
- jQuery
- Bootstrap
- Modernizr
- Normalize.css
- GreenSock

---

# Grunt

Just run 'grunt' command and start developing!

Grunt plugins:

- Watch with LiveReload
- Connect web server
- Compass
- JSHint

---

# Setting up the project

Follow these steps:

1. Navigate to the directory of the project via command-line
2. Run a command: `$ bower install`
3. Run a command: `$ npm install`
4. Run a command: `$ grunt`
